# Two Sum 
## Problem Description
 * Given an array of `integers nums` and an `integer target`, return `indices` of the **two numbers** such that they add up to `target`.
 * You may assume that each input would have ***exactly one solution***, and you may *not use the same element twice*.
 * You can return the ***answer in any order***.

## Test Cases
| Input                | Target | Expected | Alternative |
|----------------------|--------|----------|-------------|
| [2, 7, 11, 15]       | 9      | [0, 1]   | [1, 0]      |
| [3, 2, 4]            | 6      | [1, 2]   | [2, 1]      |
| [3, 3]               | 6      | [0, 1]   | [1, 0]      |
| [2, 5, 5, 11]        | 10     | [1, 2]   | [2, 1]      |
| [-1, -2, -3, -4, -5] | -8     | [2, 4]   | [4, 2]      |
| [1, 3, 4, 2]         | 6      | [2, 3]   | [3, 2]      |

## Thought Pattern One
* Start an ***outer*** loop through `List(nums)` 
```python
# Source number List/Array
nums = [2, 7, 11, 15]
# Start outer loop
for index in nums:
    print(index) 
```
* Start an ***inner*** loop through `List(nums[inner_loop_index + 1])` 
```python
# Source number List/Array
nums = [2, 7, 11, 15]
# Target value to TwoSum to
target = 9
for index, outer_value in enumerate(nums):
    # Start inner loop here
    for inner_value in range(index + 1, nums):
        print(f"Does {outer_value} + {inner_value} equal {target}?")  
```
### Pros
 * Simple and easier to understand
 * Easier to implement
### Cons
 * Becomes exponentially slower as the source number List/Array grows 

## Thought Pattern Two 
* Hash each of the numbers within the list
* Assign the index of the hash table to the hashed values
  * e.g. `hashTable[hashed_value] = 2`
* The "value" of the hashTable correlates to the index of the original list of numbers
  * Explained `originalList[hashTable[hashed_value]] = 7`
* Allowing you to avoid brute force iteration

## Milestone One 
### Goal
 * Establish a means of iterating over all potential values of the `nums List`
## Milestone Two 
### Goal
 * Develop a condition that leads towards successful result
## Milestone Three 
### Goal
 * Build the functional solution into a object-oriented manner
 * Include test cases provided by LeetCode
 * Execute code using test cases
## Milestone Four 
### Goal
 * Remove the debugging messages
 * Implementation of the test cases
 * Set the submission file for LeetCode submission
## Milestone Five
### Goal
 * Implementation of a hash table
 * Refactor the code which searches for indexes  

# Submissions
## First submission
### Milestone of submission
* Four 
### Results
* Runtime
  * 1481ms
  * Beats 34.55% of submissions
* Memory 
  * 17.4 MB
  * Beats 84.20% of submissions
## Second submission
### Milestone of submission
 * Five
### Results
 * Runtime
   * 68ms
   * Beats 43.30% of submissions
 * Memory
   * 18.33 MB
   * Beats 6.34% of submissions

# Resources
## Hash Tables
 * [Overview of Hashing](https://www.geeksforgeeks.org/hashing-data-structure/)
 * [Hashing explained](https://realpython.com/python-hash-table/)
 * [Potential Use Of Hash Table](https://www.geeksforgeeks.org/implementation-of-hash-table-in-python-using-separate-chaining/)
### Application of Hash Table
 * Caching for faster retrieval