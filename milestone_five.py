from typing import List
import unittest
from utils.Timer import Timer
from utils.HashTable import HashTable

case1   = [2, 7, 11, 15]
target1 = 9
answer1 = [0, 1]
case2   = [3, 2, 4]
target2 = 6
answer2 = [1, 2]
case3   = [3, 3]
target3 = 6
answer3 = [0, 1]
case4 =  [2, 5, 5, 11]
target4 = 10
answer4 = [1, 2]
case5 = [-1,-2,-3,-4,-5]
target5 = -8
answer5 = [2, 4]
case6 = [1, 3, 4, 2]
target6 = 6
answer6 = [2, 3]

class TwoSumTestCases(unittest.TestCase):
    def test_case1(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case1, target1), answer1)
        timer = Timer()
        self.assertEqual(submission.twoSum(case1, target1), answer1)
        timer.console_print("after test case1")

    def test_case2(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case2, target2), answer2)
        timer = Timer()
        self.assertEqual(submission.twoSum(case2, target2), answer2)
        timer.console_print("after test case2")

    def test_case3(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case3, target3), answer3)
        timer = Timer()
        self.assertEqual(submission.twoSum(case3, target3), answer3)
        timer.console_print("after test case3")

    def test_case4(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case4, target4), answer4)
        timer = Timer()
        self.assertEqual(submission.twoSum(case4, target4), answer4)
        timer.console_print("after test case4")

    def test_case5(self):
        # Negative values
        submission = Solution()
        self.assertListEqual(submission.twoSum(case5, target5), answer5)
        timer = Timer()
        self.assertEqual(submission.twoSum(case5, target5), answer5)
        timer.console_print("after test case5")

    def test_case6(self):
        # Same index output
        submission = Solution()
        self.assertListEqual(submission.twoSum(case6, target6), answer6)
        timer = Timer()
        self.assertEqual(submission.twoSum(case6, target6), answer6)
        timer.console_print("after test case6")



class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        # Set the capacity of the hash table
        ht = HashTable(len(nums))
        ht_value = 0

        # Iterate over list and add index keys using enumerate
        for key, value in enumerate(nums):
            # Intentionally assign value as the key.
            # Used to find the index/key of the original list
            ht.insert(key=value, value=key)

        for index, value in enumerate(nums):
            print(f"value={value}")
            if ht.search(target - value):
                print(f"ht.search(target - value)={ht.search(target - value)}")
                print(f"results=[{index}, {ht.search(target - value)}]")
                ht_value = ht.search(target - value)
                if ht_value != index:
                    return [index, ht_value]

        return None

        # MILESTONE FOUR FOR EXECUTION TIME
        # current = 0
        # calculation_loop = False
        #
        # for outer in nums:
        #     for inner in range(current + 1, len(nums)):
        #
        #         if outer + nums[inner] == target:
        #             calculation_loop = True
        #             break
        #
        #     if calculation_loop:
        #         break
        #     current = current + 1
        #
        # if calculation_loop:
        #     return [current, inner]
        #
        # return None


if __name__ == '__main__':
    unittest.main()
