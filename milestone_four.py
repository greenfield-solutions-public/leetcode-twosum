from typing import List
import unittest

case1   = [2, 7, 11, 15]
target1 = 9
answer1 = [0, 1]
case2   = [3, 2, 4]
target2 = 6
answer2 = [1, 2]
case3   = [3, 3]
target3 = 6
answer3 = [0, 1]


class TwoSumTestCases(unittest.TestCase):
    def test_case1(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case1, target1), answer1)
        self.assertEqual(submission.twoSum(case1, target1), answer1)

    def test_case2(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case2, target2), answer2)
        self.assertEqual(submission.twoSum(case2, target2), answer2)

    def test_case3(self):
        submission = Solution()
        self.assertListEqual(submission.twoSum(case3, target3), answer3)
        self.assertEqual(submission.twoSum(case3, target3), answer3)


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        current = 0
        calculation_loop = False

        for outer in nums:
            for inner in range(current + 1, len(nums)):

                if outer + nums[inner] == target:
                    calculation_loop = True
                    break

            if calculation_loop:
                break
            current = current + 1

        if calculation_loop:
            return [current, inner]

        return None


if __name__ == '__main__':
    unittest.main()
