# Test case
nums = [2, 7, 11, 15]
# Target value
target = 5
# Inner loop index
current = 1
# Iteration over the nums List
## Outer loop
for outer in nums:
    print(f"outer = {outer}")
    ## Inner loop
    for inner in range(current, len(nums)):
        print(f"inner = {nums[inner]}")
    # Increase counter for inner loop
    current = current + 1
