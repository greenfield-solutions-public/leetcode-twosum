# Library to make work
from typing import List

# Test cases
case1   = [2, 7, 11, 15]
target1 = 9
case2   = [3, 2, 4]
target2 = 6
case3   = [3, 3]
target3 = 6

# Class for integrating into source solution
class Solution:
    # Method for execution the primary logic
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        print(f"nums={nums}")
        print(f"target={target}")
        current = 0
        calculation_loop = False

        for outer in nums:
            print(f"outer = {outer}")
            for inner in range(current + 1, len(nums)):
                print(f"inner = {nums[inner]}")

                if outer + nums[inner] == target:
                    print(f"[{current}, {inner}]")
                    calculation_loop = True
                    break

            if calculation_loop:
                print("calculation_loop = true")
                break
            current = current + 1

        if calculation_loop:
            print(f"Return={[current, inner]}")
            return [current, inner]

        return None

# Main problem execution logic
result = Solution()
result.twoSum(nums=case1, target=target1)
result.twoSum(nums=case2, target=target2)
result.twoSum(nums=case3, target=target3)
