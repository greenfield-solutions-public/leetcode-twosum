# Test case
nums = [2, 7, 11, 15]
# Target value
target = 5
# Inner loop index
current = 1
# Flag for exit condition
calculation_loop = False
# Iteration over the nums List
## Outer loop
for outer in nums:
    print(f"outer = {outer}")
    ## Inner loop
    for inner in range(current, len(nums)):
        print(f"inner = {nums[inner]}")
        # Success condition
        if outer + nums[inner] == target:
            print(f"[{current}, {inner}]")
            # Alter flag to exit
            calculation_loop = True
            break
    # If flag is true exit looping
    if calculation_loop:
        break
    # Increase counter for inner loop
    current = current + 1
