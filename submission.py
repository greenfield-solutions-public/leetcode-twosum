class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
class HashTable:
    def __init__(self, capacity):
        self.capacity = capacity
        self.size = 0
        self.table = [None] * capacity
    def _hash(self, key):
        return hash(key) % self.capacity
    def insert(self, key, value):
        index = self._hash(key)
        if self.table[index] is None:
            self.table[index] = Node(key, value)
            self.size += 1
        else:
            current = self.table[index]
            while current:
                if current.key == key:
                    current.value = value
                    return
                current = current.next
            new_node = Node(key, value)
            new_node.next = self.table[index]
            self.table[index] = new_node
            self.size += 1
    def search(self, key):
        index = self._hash(key)
        current = self.table[index]
        while current:
            if current.key == key:
                return current.value
            current = current.next
        return None
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        ht = HashTable(len(nums))
        ht_value = 0
        for key, value in enumerate(nums):
            ht.insert(key=value, value=key)
        for index, value in enumerate(nums):
            if ht.search(target - value):
                ht_value = ht.search(target - value)
                if ht_value != index:
                    return [index, ht_value]
        return None