class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None

class HashTable:
    def __init__(self, capacity):
        self.capacity = capacity
        self.size = 0
        # Initialize as list and set number of "None" within list
        self.table = [None] * capacity
        # Example: self.table = [None,None,None,None,None,None]

    def _hash(self, key):
        # Generate a hash based on key then perform modulus operation on the capacity
        print(f"hash(key)={hash(key) % self.capacity} key={key}")
        return hash(key) % self.capacity

    def insert(self, key, value):
        index = self._hash(key)

        # If the hash table doesn't contain a value at this index, add one
        if self.table[index] is None:
            self.table[index] = Node(key, value) # Add the value of the original nums List here
            self.size += 1 # Increase size of the hash table
        # If the hash table DOES contain a Node at target index, add chained link list
        else:
            # Get pointer to table index
            current = self.table[index]

            # Iterate through chained link list within hashed table index
            while current:
                # Assign key
                if current.key == key:
                    # Assign value
                    current.value = value
                    # Exit insert method
                    return
                # Get next chained link list Node
                current = current.next
            # If no key match in chained linked in of Nodes, create new node and append to chained link list
            # Build new node
            new_node = Node(key, value)
            # Assign pointer of new node to hash table index
            new_node.next = self.table[index]
            # Assign new_node to hash table index
            self.table[index] = new_node
            # Increase the size of the hash table
            self.size += 1

    def search(self, key):
        # Get hashed tables index for value searched for
        index = self._hash(key)

        # Get current hashed tables index
        current = self.table[index]

        # While current hashed tables index contains Node(s)
        while current:
            # If found matching value
            if current.key == key:
                # Found matching value
                print(f"return value of hash table={current.value}")
                return current.value
            # Set next node in chained link list
            current = current.next

        # No key found!
        print("No key found!")
        return None


    def __len__(self):
        return self.size

    def __contains__(self, key):
        try:
            self.search(key)
            return True
        except KeyError:
            return False

    # Returns a string representation of the hash table
    def __str__(self):
        elements = []
        for i in range(self.capacity):
            current = self.table[i]
            while current:
                elements.append((current.key, current.value))
                current = current.next
        return str(elements)
