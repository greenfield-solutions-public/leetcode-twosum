import time
display_types = {"milli": 1000}


class Timer:
    def __init__(self):
        self.start = time.time()
        self.time_format = display_types["milli"]

    def now(self):
        return (time.time() - self.start) * self.time_format

    def config(self, format_time="milli"):
        self.time_format = display_types[format_time]

    def reset(self):
        self.start = time.time()

    def console_print(self, message):
        print(f"{(time.time() - self.start) * self.time_format} || {message}")